from setuptools import setup, find_packages
import MCRP

with open("README.md", "r") as f:
    long_description = f.read()
    f.close()

setup(
    name = "MCRP",
    version = MCRP.version,
    author = "seeklay",
    author_email='rudeboy@seeklay.icu',
    url = "https://gitlab.com/seeklay/MCRP",
    download_url = "https://gitlab.com/seeklay/MCRP",
    description = "Minecraft MITM proxy with function of rewriting packets, written in pure Python 3",
    long_description_content_type = "text/markdown",
    long_description = long_description,
    classifiers = [
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Intended Audience :: End Users/Desktop",
        "Natural Language :: English",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Internet"
    ],
    platforms = 'OS Independent',
    license = "GNU General Public License v3.0",
    packages = find_packages(),
    entry_points = {
        'console_scripts': ['mcwp=MCRP.mcwp:main'],
    },
    install_requires = [
        'cubelib >= 1.0.3-pre.1',
        'ruamel.yaml==0.17.21',
        'ku-proxy >= 0.2.1',
        'pycryptodome'
    ]
)
