# MCRP: Minecraft Rewrite Proxy

Minecraft MITM proxy with function of rewriting packets, written in pure [Python 3](https://www.python.org/).

[![](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) [![](https://img.shields.io/gitlab/license/cubelib/MCRP.svg)](LICENSE/) [![](https://tokei.rs/b1/gitlab/seeklay/MCRP)](#) [![](https://badgen.net/pypi/v/MCRP) ![](https://img.shields.io/pypi/dw/MCRP?style=flat&logo=pypi)](https://pypi.org/project/cubelib/) [![](https://badgen.net/gitlab/release/cubelib/MCRP)](https://gitlab.com/cubelib/MCRP/-/releases)

## About

**MCRP** provides cool interface to interact with minecraft packets goes via proxy.

# What's new in v0.2.0
* powered by ku-proxy backend
* add session-based multiclient support
* split single file into  separate modules
* rework logging levels, bind network logging with -ll
* add --ext-sore
* add&fix bug when LoginSuccess was read as uncompressed (cubelib issue #3)

## Announce
No new features will be released on base of `v0.1`.
Only security/bugfixes if needed.
Now the project will develop in the direction of replacing network engine with `ku`.
Release `v0.2` will have same functionality but written around ku.
https://gitlab.com/seeklay/ku

## Supported versions

>This note about **Play** state only! **Handshaking**, **Status**, **Login** fully supported and version-independent.

Absolutely Supported: Minecraft 1.8-1.8.9
Chat supported: Minecraft 1.12.2
For more information on supported protocols, visit the [cubelib](https://gitlab.com/cubelib/cubelib) page on gitlab, because protocol support lies with it.

## MCWP

> MineCraft Watching Proxy

MCWP is a python script written and distibuted with MCRP
It launching MCRP mitm proxy in back and setting global packet handlers to and from the server and then filtering packets with user-defined config and printing it's content in color.
*How to write filter configuration see in examples*

```bash
$> mcwp -h
 __  __   ____  ____   ____       __        __ ____
|  \/  | / ___||  _ \ |  _ \  _   \ \      / /|  _ \
| |\/| || |    | |_) || |_) |(_)   \ \ /\ / / | |_) |
| |  | || |___ |  _ < |  __/  _     \ V  V /  |  __/
|_|  |_| \____||_| \_\|_|    (_)     \_/\_/   |_| v0.2.0-pre1

usage: mcwp.py [-h] [-c config.yaml] [-v] [-l addr] [-u addr] [-d decmod]
               [-ll] [--leave-debug-journals] [--ext handlers] [-derf]
               [--ext-sore] [--devel]

Minecraft Watching Proxy

optional arguments:
  -h, --help            show this help message and exit
  -c config.yaml        Path to the YAML config file
  -v                    If passed, enables verbose logging
  -l addr               Proxy listen addr [localhost:25565] (enclose ipv6 like
                        [::])
  -u addr               Proxy upstream server addr [localhost:25575]
  -d decmod             Protocol decryption module
  -ll                   If passed, enables network debug logging
  --leave-debug-journals
                        If passed, leaves debug journals
  --ext handlers        Module with register() containing external handlers
  -derf                 Disable entities resolving and following
  --ext-sore            Show rejected packets
  --devel
```
## Requirements

* Python 3.7 and above (tested and developed under `Python 3.7.9`)
* [cubelib](https://gitlab.com/cubelib/cubelib) >= 1.0.3-pre.1
* [ruamel.yaml](https://pypi.org/project/ruamel.yaml/) == 0.17.21
* [pycryptodome](https://pypi.org/project/pycryptodome/)
* [ku](https://gitlab.com/seeklay/ku-proxy) >= 0.2.1

## Installation

MCRP is published in [PyPI](https://pypi.org/project/cubelib/), so latest release can be installed with one simple command:
```bash
pip install -U mcrp
```

or bleeding edge from git sources (unstable) (may not work at all!):

```bash
git clone https://gitlab.com/seeklay/MCRP.git
cd mcrp/
pip3 install .
```
## Docs
# Check out our new cool pretty terrifying amazing documentation [HERE](https://mcrp.seeklay.pp.ua)

## Usage examples
*if you set a domain name as a listen or an upstream address, ipv4 will used, ipv6 works only with addresses in this notation: [::1]. if the port is not specified, the default will be 25565. if upstream/listen addr not specified 25565->25575 at localhost will be used by default*
> default configured mcwp and normal ping sequence (mcsr used)
```bash
$> mcwp -c examples\conf_blacklist.yaml
 __  __   ____  ____   ____       __        __ ____
|  \/  | / ___||  _ \ |  _ \  _   \ \      / /|  _ \
| |\/| || |    | |_) || |_) |(_)   \ \ /\ / / | |_) |
| |  | || |___ |  _ < |  __/  _     \ V  V /  |  __/
|_|  |_| \____||_| \_\|_|    (_)     \_/\_/   |_| v0.2.0-pre1

[07/08/2023 11:18:42 PM] [INFO] MCRP:  Running MCRP/0.2.0-pre1 (cubelib version 1.0.9)
[07/08/2023 11:18:42 PM] [INFO] MCRP:  Proxying config is: 127.0.0.1:25565 -> 127.0.0.1:25575
[07/08/2023 11:18:42 PM] [INFO] MCRP:  Registred direct handlers list[1]:
[07/08/2023 11:18:42 PM] [INFO] MCRP:  [1]    <class 'cubelib.proto.version_independent.ServerBound.Handshaking.Handshake'>
[07/08/2023 11:18:42 PM] [INFO] MCRP:  Registred relative handlers list[0]:

[07/08/2023 11:18:44 PM] [INFO] MCRP:  #2006377812680 ready for trasmission
[07/08/2023 11:18:44 PM] [INFO] root:  ServerBound   Handshaking.Handshake(ProtoVer=-1, ServerName='localhost', ServerPort=25565, NextState=state.Status)
[07/08/2023 11:18:44 PM] [INFO] root:  Handshaking.Handshake(ProtoVer=-1, ServerName='localhost', ServerPort=25565, NextState=state.Status) -> Handshaking.Handshake(ProtoVer=-1, ServerName='127.0.0.1', ServerPort=25575, NextState=state.Status)
[07/08/2023 11:18:44 PM] [INFO] root:  ServerBound   Status.Request()
[07/08/2023 11:18:44 PM] [INFO] root:  ClientBound   Status.Response(JsonRsp='{"description":"A Minecraft Server","players":{"max":20,"online":0},"version":{"name":"Spigot 1.8.8","protocol":47}}')
[07/08/2023 11:18:44 PM] [INFO] root:  ServerBound   Status.Ping(Uniq=1337)
[07/08/2023 11:18:44 PM] [INFO] root:  ClientBound   Status.Pong(Uniq=1337)
[07/08/2023 11:18:44 PM] [INFO] MCRP:  #2006377812680 connection_lost by server due to None
```
See [**examples/**](examples/) for MCRP and MCWP external handlers usage details

## Protocol decryption
### MCWP
For protocol decryption enabling you need user to use your instance of [yggdrasil-server](https://gitlab.com/seeklay/yggdrasil-server) 
Add `-d md` arg to mcwp command to use standard yggdrasil server decryption module
This module will spoof server publickey, read shared secret and spoof session on sessionserver.
Then you will see decrypted packets.

### MCRP
If you want to manipulate encrypted packets from your script you need to add `decryptor` keyword argument to MCRewriteProxy constructor like this:

```python
proxy = MCRewriteProxy(("localhost", 25565), ("localhost", 25575), logging.INFO,
	decryptor=decryptor_class)
```
where decryptor_class is heir of `MCRP.ProtocolDecryptor` like `MCWPDecryptor` in md.py, oh yes you can import `MCWPDecryptor` from md.py and use it here too

## TODO
- [x] Rewrite networking with [ku](https://gitlab.com/seeklay/ku)
- [ ] еще ченить можно добавить =P

## Author
**[seeklay](https://gitlab.com/seeklay)**

## License
**[GNU GPL 3.0](LICENSE)**
