"""
    Example of commands blocking proxy
"""

from MCRP import MCRewriteProxy, Relative
import logging
logging.basicConfig()

KICK = True

if __name__ == "__main__":
    proxy = MCRewriteProxy(("localhost", 25565), ("localhost", 25575), logging.INFO)

    @proxy.on(Relative.ServerBound.Play.ChatMessage)
    def handler(pack):    
        if pack.Message[0] == "/":
            if KICK:
                proxy.Client.send(proxy.PROTOCOL.ClientBound.Play.Disconnect('commands.is.not.allowed.at.this.server').build(proxy.COMPRESSION_THR))

            return False # Prevent this packet from being sent to the server

    proxy.join()
