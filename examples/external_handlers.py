"""
    My small colle.. ah oh
    external handlers!
"""

import cubelib
from MCRP import Relative, MCRewriteProxy, mcsession
import json
import time
import random
from traceback import format_exc

def howoften(fun, data = {}):
    data[hash(fun)] = []
    data[hash(fun)].append(0)
    data[hash(fun)].append(time.time())

    def wrap(*args, **kwargs):
        data[hash(fun)][0] += 1
        d = time.time() - data[hash(fun)][1]
        if (d >= 1):
            c = data[hash(fun)][0] / d
            print(f"{fun.__name__} is called {c} tps")
            data[hash(fun)][1] = time.time()
            data[hash(fun)][0] = 0
        return fun(*args, **kwargs)
    return wrap

def register(proxy: MCRewriteProxy):

    @proxy.on(Relative.ServerBound.Play.ChatMessage)
    def handler(sess, pack: cubelib.p.Packet):    
        cmd = pack.Message.split(" ")
        if not hasattr(sess, 'cyi'):
            sess.cyi = False
        if not hasattr(sess, 'selected_entity'):
            sess.selected_entity = None

        if cmd[0] == "/ab":
            
            # Send message to the player's actionbar
            
            pp = sess.protocol.ClientBound.Play.ChatMessage
            p = pp(cmd[1:], pp.Position.Actionbar)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server

        elif cmd[0] == "/meta":

            # Add metadata to an entity

            metadata = []
            for i in range(1, len(cmd[1:]), 3):
                metadata.append({
                     "index": int(cmd[i]),
                     "type": int(cmd[i + 1]),
                     "value": cmd[i + 2]
                })


            p = sess.protocol.ClientBound.Play.EntityMetadata(sess.selected_entity, metadata)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server

        elif cmd[0] == "/plhaf":

            # Set header and footer (tab)

            header = json.dumps({"text": cmd[1]})
            footer = json.dumps({"text": cmd[2]})

            p = sess.protocol.ClientBound.Play.PlayerListHeaderAndFooter(header, footer)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/cgs":

            # Change game state

            pp = sess.protocol.ClientBound.Play.ChangeGameState
            p = pp(pp.Reason(int(cmd[1])), float(cmd[2]))
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/es":

            # Set entity status

            p = sess.protocol.ClientBound.Play.EntityStatus(sess.selected_entity, int(cmd[2]))
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server

        
        if cmd[0] == "/ae":

            # Attach entity

            p = sess.protocol.ClientBound.Play.AttachEntity(int(cmd[1]), int(cmd[2]), bool(cmd[3] if len(cmd) == 4 else False))
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/sm":

            # Spawn Mob

            try:
                mob = getattr(sess.protocol.Mob, cmd[1])
            except AttributeError:
                sess.chatClient(f"Mob with name {cmd[1]} not found!")
                return False # Prevent this packet from being sent to the server

            eid = random.randint(5999, 12000)
            p = sess.protocol.ClientBound.Play.SpawnMob(eid, mob, sess.PLAYERX, sess.PLAYERY, sess.PLAYERZ, 0, 0, 0)#, 0, 0, 0, [])
            sess.sendClient(p)
            sess.chatClient(f"Spawned {mob} with EntityID[{eid}]")

            return False # Prevent this packet from being sent to the server
                    
        if cmd[0] == "/de":

            # Destroy Entities

            if len(cmd) < 2:
                entities = list(sess.dgim.entities.keys())
            else:
                entities = [int(e) for e in cmd[1:]]

            p = sess.protocol.ClientBound.Play.DestroyEntities(entities)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/erm":

            # Entity relative move

            p = sess.protocol.ClientBound.Play.EntityRelativeMove(sess.selected_entity, int(cmd[1]), int(cmd[2]), int(cmd[3]), True)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/spinner":

            # Tasks example

            if not sess.dgim:
                sess.chatClient('Dgim disabled!')
                return

            sess.spinner_angle = 0
            sess.elock = True
            def task():
                if sess.spinner_angle >= 255:
                    sess.spinner_angle = 0

                for eid, e in sess.dgim.entities.items():
                    if e.EType == 'Player':
                        continue
                    
                    p = sess.protocol.ClientBound.Play.EntityHeadLook(eid, sess.spinner_angle if eid % 2 else 255 - sess.spinner_angle)
                    sess.sendClient(p)
                    p = sess.protocol.ClientBound.Play.EntityLook(eid, 0, random.randint(0, 20), True)
                    sess.sendClient(p)

                sess.spinner_angle += int(cmd[1])

            sess.tasks.append(task)

            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/cc":

            # Clear tasks

            sess.tasks = []
            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/rr":
            sess.rrc = 0
            def task():
                if sess.rrc >= 255:
                    sess.rrc = 0
                p = sess.protocol.ClientBound.Play.EntityLook(sess.selected_entity, 0, sess.rrc, True)
                sess.rrc += int(cmd[1])
                sess.sendClient(p)
                # sess.chatClient(str(sess.rrc))

            sess.tasks.append(task)
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/reload":
            sess.chatClient("Begin handlers reloading")
            try:
                sess.proxy.reload(sess) # initiate ext handlers reloading
            except:
                sess.chatClient("Failed to reload handlers!")
                return

            sess.chatClient("Successful reloaded protocol handlers")

            sess.tasks = []
            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/break":
            #import random
            def task():
                base = 100
                size = 5
                for i in range(-size, size):
                    for j in range(-size, size):
                        pos = (int(sess.PLAYERX + j), int(sess.PLAYERY - 1), int(sess.PLAYERZ + i))
                        #print(pos)
                        p = sess.protocol.ClientBound.Play.BlockBreakAnimation(base + i*3 + j*2, pos, random.randint(0, 9))
                        sess.sendClient(p)

            sess.tasks.append(task)                      
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/tb":

            # Finally, lightning

            p = sess.protocol.ClientBound.Play.SpawnGlobalEntity(1000, 1, sess.PLAYERX, sess.PLAYERY, sess.PLAYERZ)
            sess.sendClient(p)
            # molniya

            p = sess.protocol.ClientBound.Play.SoundEffect(sess.protocol.Sound.AMBIENT_WEATHER_THUNDER, int(sess.PLAYERX*8), int(sess.PLAYERY*8), int(sess.PLAYERZ*8), 1, 63)
            sess.sendClient(p)
            # grom

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/se":
            
            # Play random sound effect

            if len(cmd) < 2:
                sound = sess.protocol.Sound(random.randint(0, len(sess.protocol.Sound)))
            else:
                sound = sess.protocol.Sound.from_id(cmd[1])

            sess.chatClient(f"SoundEffect playing: {sound}")

            p = sess.protocol.ClientBound.Play.SoundEffect(sound, int(sess.PLAYERX*8), int(sess.PLAYERY*8), int(sess.PLAYERZ*8), 1, 63)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/signtimer":
            # Timer on the all signs

            if not hasattr(sess, "signs"):
                sess.chatClient("You have not created any sign yet!")
                return False # Prevent this packet from being sent to the server

            sess.signtimer = 0
            def task():
                for sign in sess.signs:
                    timer = json.dumps({"text": f"Elapsed: {round(sess.signtimer, 2)}s"})
                    p = sess.protocol.ClientBound.Play.UpdateSign(sign, "", timer, "", "")
                    sess.sendClient(p)

                sess.signtimer += 0.0499

            sess.tasks.append(task)   
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/colorfulwolfs" or cmd[0] == "/cw":

            # Changes the collar color of all tamed wolves

            if not sess.dgim:
                sess.chatClient('Dgim disabled!')
                return

            sess.wolfcolor = -128

            def task():           
                if sess.wolfcolor >= 128:
                    sess.wolfcolor = 0

                meta = [{
                    "index": 20,
                    "type": 0,
                    "value": int(sess.wolfcolor)
                }]

                for e, v in sess.dgim.entities.items():           
                    if v.EType != sess.protocol.Mob.WOLF:
                        continue

                    p = sess.protocol.ClientBound.Play.EntityMetadata(e, meta)                
                    sess.sendClient(p)

                sess.wolfcolor += 0.3

            sess.tasks.append(task)  
            return False # Prevent this packet from being sent to the server    
       
        if cmd[0] == "/so":

            # Spawn object
            
            try:
                obj = getattr(sess.protocol.Object, cmd[1])
            except AttributeError:
                sess.chatClient(f"Object with name {cmd[1]} not found!")
                return

            eid = random.randint(5999, 12000)
            p = sess.protocol.ClientBound.Play.SpawnObject(eid, obj, int(sess.PLAYERX), int(sess.PLAYERY), int(sess.PLAYERZ), 0, 10, int(cmd[2]), 0, 0, 0)
            sess.sendClient(p)
            sess.chatClient(f"Spawned {obj} with EntityID[{eid}]")
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/respawn" or cmd[0] == "/re":

            # Respawn

            p = sess.protocol.ClientBound.Play.Respawn(0, 0, 1, "default")
            sess.sendClient(p)

            p = sess.protocol.ClientBound.Play.PlayerPositionAndLook(sess.PLAYERX, sess.PLAYERY, sess.PLAYERZ, sess.PLAYERYAW, sess.PLAYERPITCH, 0)
            sess.sendClient(p)

            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/le" or cmd[0] == "/listentities":

            # List all spawned entities

            if not sess.dgim:
                sess.chatClient('Dgim is disabled!')
                return False # Prevent this packet from being sent to the server
            
            sess.chatClient(str(sess.dgim.entities))
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/elock":

            # Change elock state

            sess.elock = not sess.elock
            sess.chatClient(f"Elock state: {sess.elock}")
            return False # Prevent this packet from being sent to the server

        if cmd[0] == "/ah":

            # Print active handlers list

            output = ""
            for handler_type, handlers in sess.handlers.items():
                output += f"{'RELATIVE' if handlers[0] else 'DIRECT'} {handler_type}[{len(handlers[1])}]\n"
            sess.chatClient(output)
            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/cyi":

            # Enable interactive ingame python interaction

            sess.cyi = not sess.cyi
            sess.chatClient(f"CYI: {sess.cyi}")
            return False # Prevent this packet from being sent to the server

        if pack.Message[0] == "/" and sess.cyi:

            # CYI handler

            try:
                output = str(eval(pack.Message[1:]))
            except:
                output = format_exc()
            
            sess.chatClient(output)
            return False # Prevent this packet from being sent to the server
        
        if cmd[0] == "/f":

            # Finalize session
            p = sess.protocol.ClientBound.Play.Disconnect("{'text': 'CU', 'color': 'blue'}")
            sess.sendClient(p)
            sess.terminate()
            return False # Prevent this packet from being sent to the server

        return pack

    @proxy.on(Relative.ServerBound.Play.Player)
    @proxy.on(Relative.ServerBound.Play.PlayerPositionAndLook)
    @proxy.on(Relative.ServerBound.Play.PlayerPosition)
    @proxy.on(Relative.ServerBound.Play.PlayerLook)
    # @howoften uncomment in case of performance(timings)[tps] check
    def handler(sess, pack):     
        if hasattr(pack, "X"):
            sess.PLAYERX = pack.X
        if hasattr(pack, "FeetY"):
            sess.PLAYERY = pack.FeetY
        if hasattr(pack, "Z"):
            sess.PLAYERZ = pack.Z
        if hasattr(pack, "Yaw"):
            sess.PLAYERYAW = pack.Yaw
        if hasattr(pack, "Pitch"):
            sess.PLAYERPITCH = pack.Pitch
        if hasattr(pack, "OnGround"):
            sess.PLAYERG = pack.OnGround

        # 20 times per second
        # once per tick
        # 1 tick = 0.05s
        # 1s = 20 ticks

        if not hasattr(sess, "tasks"):
            sess.tasks = []
        else:
            for task in sess.tasks:
                task()

    @proxy.on(Relative.ServerBound.Play.UseEntity)
    def handler(sess, pack):
        
        if pack.Type == sess.protocol.ServerBound.Play.UseEntity.Type.InteractAt:
            sess.selected_entity = pack.Target
            sess.chatClient(f"Current target isset to: {pack.Target}")
    
    @proxy.on(Relative.ClientBound.Play.EntityHeadLook)
    @proxy.on(Relative.ClientBound.Play.EntityLook)
    def handler(sess, pack):
        if not hasattr(sess, "elock"):
            sess.elock = False

        if sess.elock:
            return False # Prevent this packet from being sent to the server

    @proxy.on(Relative.ClientBound.Play.PluginMessage)
    def handler(sess: mcsession, pack):
        
        # F3 Branding (Looks useless on 1.8.8)

        if pack.Channel == "MC|Brand":
            pack.Data = cubelib.types.String.build("via/MCRP")
        return pack
    
    @proxy.on(Relative.ClientBound.Play.UpdateSign)
    def handler(sess, pack):

        # Signs collector (Signtimer)

        if not hasattr(sess, "signs"):
            sess.signs = set()
        
        sess.signs.add(pack.Location)
