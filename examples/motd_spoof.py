"""
    Example of MOTD spoofing proxy
"""

from MCRP import MCRewriteProxy
from base64 import b64encode
import cubelib
import json

MOTD = {
    "description": "",
    "players": {
        "max": -1,
        "online": -1,
        "sample": [
            {
                "name": "",
                "id": "ffffffff-ffff-ffff-ffff-ffffffffffff"
            }
        ]
    },
    "version": {
        "name": "",
        "protocol": 47
    },
    "favicon": "data:image/png;base64," + icon
}

if __name__ == "__main__":

    with open('icon.png', 'rb') as f:
        icon = b64encode(f.read()).decode()
        f.close()
    
    proxy = MCRewriteProxy(("localhost", 25565), ("localhost", 25575), verbose=True)
    
    print = proxy.logger.debug # print with mcrp deubug logging

    @proxy.on(cubelib.proto.ServerBound.Status.Request)
    def handler(pack):
        print(f"spoofing motd")
        proxy.Client.sendall(cubelib.proto.ClientBound.Status.Response(json.dumps(MOTD)).build(-1))    

    @proxy.on(cubelib.proto.ClientBound.Status.Response)
    def handler(pack):    
        return False

    @proxy.on(cubelib.proto.ServerBound.Status.Ping)
    def handler(pack):
        print(f"spoofing ping")
        proxy.Client.sendall(cubelib.proto.ClientBound.Status.Pong(pack.Uniq).build(-1))    

    @proxy.on(cubelib.proto.ClientBound.Status.Pong)
    def handler(pack):    
        return False

    proxy.join()
