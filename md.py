"""
    Protocol decryptor module for MCWP and yggdrasil-server
"""

from MCRP import ProtocolDecryptor

from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_v1_5
import os

from hashlib import sha1

import requests

class MCWPDecryptor(ProtocolDecryptor):
    """
        Protocol Decryptor Prototype
    """

    name: str = "Yggdrasil-Server-DecMod"
    version: str = "v0.1.1"
    
    @staticmethod
    def mcsha1(data: bytes):
        return format(int.from_bytes(sha1(data).digest(), byteorder='big', signed=True), 'x')
    
    def __init__(self, logger):        

        self.logger = logger
        self.logger.debug("Generating 1024 RSA key...")
        self.key = RSA.generate(1024)
        self.downstream_cipher = PKCS1_v1_5.new(self.key)

    def EncryptionRequest(self, server_id: str, public_key: bytes, verify_token: bytes) -> (str, bytes, bytes):
        """
        External Encryption Request handler for decryption setup purposes

        Args:
            server_id (str): EncryptionRequest.ServerID
            public_key (bytes): EncryptionRequest.PublicKey
            verify_token (bytes): EncryptionRequest.VerifyToken
        
        Returns:
            server_id (str): New server id
            public_key (bytes): New public key
            verify_token (bytes): New verify token
        """                
        self.server_public_key = public_key
        self.upstream_cipher = PKCS1_v1_5.new(RSA.import_key(self.server_public_key))
        self.server_verify_token = verify_token
        return "", self.key.publickey().export_key('DER'), os.urandom(4)
    
    def EncryptionResponse(self, shared_secret: bytes, verify_token: bytes) -> (bytes, bytes, bytes):
        """
        External Encryption Response handler for decryption setup purposes

        Args:
            shared_secret (bytes): EncryptionResponse.SharedSecret
            verify_token (bytes): EncryptionResponse.VerifyToken
        
        Returns:
            shared_secret (bytes): Plain shared secret
            shared_secret (bytes): Encrypted shared secret
            verify_token (bytes): Encrypted verify token
        """                  
        shared_secret = self.downstream_cipher.decrypt(shared_secret, os.urandom(16))

        their_session = self.mcsha1(shared_secret + self.server_public_key)
        our_session = self.mcsha1(shared_secret + self.key.publickey().export_key("DER"))
        print(our_session, their_session)
        pload = {
            "their": their_session,
            "our": our_session
        }
        # spoof session serverId on "session server"...
        req = requests.post("https://sessionserver.mojang.com/session/minecraft/falsify", json=pload, verify=False)
        if req.status_code != 204:
            self.logger.warning("Authserver refused to spoof our session... looks like we screwed up")

        return shared_secret, self.upstream_cipher.encrypt(shared_secret), self.upstream_cipher.encrypt(self.server_verify_token)
