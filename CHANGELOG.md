# v0.1.0-RELEASE
* First project upload
* MCRP
* MCWP

# v0.1.1 (22 Jan 2023)
* fix packet skip mechanism
* add handling new behavior for licensed clients connection (with encryption) (mcwp)

# v0.1.2 (25 Jan 2023)
* fix logging
* make it possible to start capturing without filtering
* add socket bind failed exception handler

# v0.1.3 (26 Jan 2023)
* add packet length limiter for log messages (mcwp)
* add dedicated filtering mode parameters for client and server bounds (mcwp)

# v0.1.4 (28 Jan 2023)
* fix case when filter logging "None" in list of filtered packets when this type of packet not supported by cubelib
* add ipv6 support

# v0.1.5 (31 Jan 2023)
* fix accidentally buffer globalization
* rewrite `host:port` separation method (mcwp)
* change some log messages

# v0.1.6 (18 Feb 2023)
* fix issue #1 (Add MC|PingHost handler to mcrp)

# v0.1.7-pre1 (20 Feb 2023)
* add new acceptable return type for a handlers: packet
* add protocol early decryption feature and decryptor api class
* add protocol debug journals support (.mcdj)

# v0.1.7(8 Mar 2023)
* add protocol decryption support
  
# v0.1.8 (19 Mar 2023)
* rewrite addr parsing method
  - now can be used like this: `mcwp -u localhost` but the domains are still connects over ipv4 only. 
* add servername autospoof in handshake packet **[hot]**
  - now works with upstream server behind minecraft protection with servername check
* fix broken behaviour of pass through mechanism when `LoginStart` is sent with a `Handshake` in the same buffer
* update debug journals structure
* add upstream connection timeout

# v0.1.9 (21 Mar 2023) HOTFIX
* fix local variable duplication
* fix journal setenckey when not journaling
* fix dumb timeouting, my bad

# v0.1.10 (21 Jun 2023) I'M SO TIRED oF Ths
* add entities resolving (MCWP)
* add metadata resolving (MCWP)
* add external handlers support (MCWP)
* add external handlers reloading mechanism (MCWP)
* add `-derf` for disabling entities resolving (MCWP)
* add handler for rewrited packets (MCRP)
* add runtime handlers unloading/reloading mechanisms (MCRP)
* add external handlers example (MCWP)
* add statistics printing to the screen and game client on session close (MCRP)
* fix disconnect packet when proxy closing with enabled encryption or unsupported proto

# v0.2.0 KU-PROXY
* powered by ku-proxy backend
* add session-based multiclient support
* split single file into  separate modules
* rework logging levels, bind network logging with -ll
* add --ext-sore
* add&fix bug when LoginSuccess was read as uncompressed (cubelib issue #3)